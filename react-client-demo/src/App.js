import React from 'react';
import './App.css';

import { Route,BrowserRouter  } from 'react-router-dom';

import Navigation from './Component/Nav/Navigation';

import ComaparePage from './Container/Compare/Compare';
import ComputePage from './Container/Compute/Compute';
import HelpPage from './Container/Help/Help';


function App() {
  return (
     <React.Fragment> 
      <BrowserRouter >
      <Navigation /> 
      <main>
        <Route path="/" component={ComaparePage} exact />
        <Route path="/compute" component={ComputePage} /> 
        <Route path="/help" component={HelpPage} /> 

      </main>
      </BrowserRouter >

    </React.Fragment>    
  ); 
}  

export default App;
