package com.cloud.compareService.repository;

import com.cloud.compareService.model.InstanceType;
import com.cloud.compareService.model.LeaseTerm;
import com.cloud.compareService.model.OfferingClass;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface LeaseTermRepository extends JpaRepository<LeaseTerm, Integer>
    , JpaSpecificationExecutor<LeaseTerm>,BaseRepository<LeaseTerm>{

    public LeaseTerm findById(int Id);
    
    public List<LeaseTerm> findAll();

}
