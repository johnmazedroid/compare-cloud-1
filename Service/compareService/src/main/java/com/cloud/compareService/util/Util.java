package com.cloud.compareService.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class Util {

    public static Object getVOfromJsonString(String jsonString, String className)
            throws IOException, ClassNotFoundException {

        ObjectMapper mapper = new ObjectMapper();
        Object obj=null;
        obj = mapper.readValue(jsonString, Class.forName(className));
        return obj;

    }

    public static boolean stringNotEmptyOrNull(String st) {
        return st != null && !st.trim().isEmpty();
    }
}
