package com.cloud.compareService.vo;


import java.io.Serializable;
import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class ComputeResourceInputFieldVO implements Serializable {
    public String[] value;
    public String search;
    public String key;

    @Override
    public String toString() {
        return "ComputeResourceInputField{" +
            "value=" + Arrays.toString(value) +
            ", search='" + search + '\'' +
            ", key='" + key + '\'' +
            '}';
    }
}
