package com.cloud.compareService.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;




@AllArgsConstructor
@Builder
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "server_pricing")
public class ServerPricing implements Serializable {

    @Id
    private int serverPricingId;
    private float hourlyPrice;
    @JsonIgnore
    private Date createdDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="lease_term_mst_id", nullable=true)
    private LeaseTerm leaseTerm;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="offering_class_mst_id", nullable=true)
    private OfferingClass offeringClass;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="payment_option_mst_id", nullable=true)
    private PaymentOption paymentOption;


    private int serverAttributesId;

    @Transient
    private float price;



}



