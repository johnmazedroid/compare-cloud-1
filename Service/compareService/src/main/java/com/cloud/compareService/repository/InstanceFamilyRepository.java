package com.cloud.compareService.repository;

import com.cloud.compareService.model.CloudProvider;
import com.cloud.compareService.model.Instance;
import com.cloud.compareService.model.InstanceFamily;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface InstanceFamilyRepository extends JpaRepository<InstanceFamily, Integer>
    , JpaSpecificationExecutor<InstanceFamily>,BaseRepository<InstanceFamily>{

    public InstanceFamily findById(int Id);
    
    public List<InstanceFamily> findAll();

}
