package com.cloud.compareService.dao;

import com.cloud.compareService.model.LeaseTerm;
import com.cloud.compareService.model.Region;
import com.cloud.compareService.repository.LeaseTermRepository;
import com.cloud.compareService.repository.OfferingClassRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LeaseTermDaoImpl extends BaseDao<LeaseTerm>{

    @Autowired
    public LeaseTermDaoImpl(LeaseTermRepository leaseTermRepository){
        super(leaseTermRepository);
    }

    public LeaseTerm save(LeaseTerm leaseTerm) {
        return repository.save(leaseTerm);
    }

    public List<LeaseTerm> findAll() {
        return repository.findAll();
    }

}




