package com.cloud.compareService.service;
import com.cloud.compareService.client.AuthClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

@Service
public class AuthService {

    @Autowired
    private AuthClient authClient;

    public boolean isUserValid(String name) {
        boolean isValid = authClient.isUserValid(name);
        if (!isValid) {
            throw new HttpClientErrorException(HttpStatus.FORBIDDEN, "Invalid user");
        }
        return isValid;
    }
}
