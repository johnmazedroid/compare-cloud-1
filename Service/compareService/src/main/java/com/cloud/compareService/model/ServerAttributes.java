package com.cloud.compareService.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;


@AllArgsConstructor
@Builder
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "server_attributes")
public class ServerAttributes implements Serializable {

    @Id
    private int serverAttributesId;
    private int virtualCpu;
    private float memory;
    private float defaultStorage;
    @JsonIgnore
    private Date createdDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="cloud_provider_id", nullable=true)
    private CloudProvider cloudProvider;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="operating_system_mst_id", nullable=true)
    private OperatingSystem operatingSystem ;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="region_mst_id", nullable=true)
    private Region region ;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="instance_mst_id", nullable=true)
    private Instance instance ;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="instance_type_mst_id", nullable=true)
    private InstanceType instanceType ;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="instance_family_mst_id", nullable=true)
    private InstanceFamily instanceFamily ;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="processor_mst_id", nullable=true)
    private Processor processor;




}



