package com.cloud.compareService.util;

import lombok.Getter;
import lombok.Setter;


public enum


SearchMapping {
  cloudProviderMst("cloudProvider","cloudProvider", "cloudProviderId"),
  regionMst("region","cloudProvider", "cloudProviderId"),
  processorMst("processor","cloudProvider", "cloudProviderId"),
  instanceMst("instance","cloudProvider", "cloudProviderId"),
  instanceTypeMst("instanceType","cloudProvider", "cloudProviderId"),
  instanceFamilyMst("instanceFamily","cloudProvider", "cloudProviderId"),
  operatingSystemMst("operatingSystem","cloudProvider", "cloudProviderId"),
  leaseTermMst("leaseTerm","cloudProvider", "cloudProviderId"),
  offeringClassMst("offeringClass","cloudProvider", "cloudProviderId"),
  paymentOptionMst("paymentOption","cloudProvider", "cloudProviderId"),
  region("ServerAttributes","region", "regionGroupMapped"),
  cloudProvider("ServerAttributes","cloudProvider", "name"),
  instance("ServerAttributes","instance", "instanceName"),
  instanceType("ServerAttributes","instanceType","instanceTypeMapped"),
  instanceFamily("ServerAttributes","instanceFamily","instanceFamilyMapped"),
  processor("ServerAttributes","processor","processorFamilyMapped"),
  operatingSystem("ServerAttributes","operatingSystem","operatingSystemMapped"),
  noOfServers(Constant.N_A,Constant.N_A, "noOfServers"),
  workloadId(Constant.N_A,Constant.N_A, "workloadId"),
  usagePerMonth(Constant.N_A,Constant.N_A, "usagePerMonth"),
  tcoTerm(Constant.N_A,Constant.N_A, "tcoTerm"),
  memory("ServerAttributes",Constant.N_A, "memory"),
  defaultStorage("ServerAttributes",Constant.N_A, "defaultStorage"),
  virtualCpu("ServerAttributes",Constant.N_A, "virtualCpu"),
  leaseTerm("ServerPricing","leaseTerm", "leaseTermMapped"),
  offeringClass("ServerPricing","offeringClass", "offeringClassMapped"),
  paymentOption("ServerPricing","paymentOption", "paymentOptionMapped"),
  serverAttribute("ServerPricing",Constant.N_A, "serverAttributesId");


  public String entity;
  public String attribute;
  public String parent;
  SearchMapping(String parent,String entity,String attribute) {
    this.entity = entity;
    this.attribute=attribute;
    this.parent=parent;
  }

}


