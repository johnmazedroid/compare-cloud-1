package com.cloud.compareService.repository;

import com.cloud.compareService.model.LeaseTerm;
import com.cloud.compareService.model.OfferingClass;
import com.cloud.compareService.model.OperatingSystem;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface OfferingClassRepository extends JpaRepository<OfferingClass, Integer>
    , JpaSpecificationExecutor<OfferingClass>,BaseRepository<OfferingClass>{

    public OfferingClass findById(int Id);
    
    public List<OfferingClass> findAll();

}
