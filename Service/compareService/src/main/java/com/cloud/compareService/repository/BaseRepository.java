package com.cloud.compareService.repository;

import com.cloud.compareService.model.Region;
import java.util.List;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

@Repository
public interface BaseRepository<E>   {

  public List<E> findAll(Specification spec);
  public List<E> findAll();
  public E findById(int Id);
  public E save(E e);

}
