package com.cloud.compareService.client;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("auth")
public interface AuthClient {
    @RequestMapping(method = RequestMethod.GET, value = "/auth/validate")
    boolean isUserValid(@RequestParam(name = "name") String name);
}

