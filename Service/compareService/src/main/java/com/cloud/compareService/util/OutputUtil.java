package com.cloud.compareService.util;

import com.cloud.compareService.model.ServerPricing;
import com.cloud.compareService.vo.ComputeResourceOutputVO;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;

public class OutputUtil {

    public static void populateChoice(List<ComputeResourceOutputVO> computeResourceOutputVOList) {
        float lowestPrice=getSALowestPrice(computeResourceOutputVOList);
        for(ComputeResourceOutputVO computeResourceOutputVO: computeResourceOutputVOList){

                if ((computeResourceOutputVO.getTotalPrice() == lowestPrice)) {
                    computeResourceOutputVO.setChoice("Selected");
                } else {
                    computeResourceOutputVO.setChoice("Optional");
                }

        }
    }

  /*  public static float getSPLowestPrice(List<ComputeResourceOutputVO> computeResourceOutputVOList) {
        float lowestPrice=Float.MAX_VALUE;
        for(ComputeResourceOutputVO computeResourceOutputVO: computeResourceOutputVOList){
            for(ServerPricing sp: computeResourceOutputVO.getServerPricing() ){
                if(sp.getPrice() < lowestPrice){
                    lowestPrice = sp.getPrice();
                }
            }
        }
        return lowestPrice;
    }

   */

    public static float getSALowestPrice(List<ComputeResourceOutputVO> computeResourceOutputVOList) {
        float lowestTotalPrice=Float.MAX_VALUE;
        for(ComputeResourceOutputVO computeResourceOutputVO: computeResourceOutputVOList){
                if(computeResourceOutputVO.getTotalPrice() < lowestTotalPrice){
                    lowestTotalPrice = computeResourceOutputVO.getTotalPrice() ;
                }
        }
        return lowestTotalPrice;
    }

    public static void populatePrice(ComputeResourceOutputVO computeResourceOutputVO) {
        float totalPrice=computeResourceOutputVO.getTotalPrice();
        for(ServerPricing sp: computeResourceOutputVO.getServerPricing() ){
            float price=sp.getHourlyPrice()* 24 *30*12;
            sp.setPrice(price);
            if(totalPrice==0  || totalPrice>=price){
                computeResourceOutputVO.setTotalPrice(price);
            }
        }
    }
}
