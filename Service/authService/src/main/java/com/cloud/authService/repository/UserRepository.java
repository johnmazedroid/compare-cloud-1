package com.cloud.authService.repository;

import com.cloud.authService.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    public User findById(int Id);

    @Query(value=" select * from users.user \n" +
            "WHERE LOWER(name) LIKE :name", nativeQuery=true)
    public List<User> findByName(@Param("name") String name );

    @Query(value=" SELECT IF(  EXISTS(SELECT * FROM users.user WHERE LOWER(name) =:name AND password = :password),'true','false' )AS result", nativeQuery=true)
    public boolean validateUSer(@Param("name") String name,@Param("password") String password );

    public List<User> findAll();

}


