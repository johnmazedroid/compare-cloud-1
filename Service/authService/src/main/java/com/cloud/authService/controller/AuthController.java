package com.cloud.authService.controller;

import com.cloud.authService.model.User;
import com.cloud.authService.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;


import java.util.List;

@Slf4j
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    AuthService authService;

    @Value("${spring.datasource.url}")
    private String databaseUrl;



    @GetMapping("/user/all")
    public List<User> getUsers() {
        return authService.getUsers();
    }

    @RequestMapping(method=RequestMethod.GET, path="/user")
    @ResponseBody
    public User getUser(@RequestParam(name = "id") int id ) {
        return authService.getUser(id);
    }

    @RequestMapping(method=RequestMethod.POST, path="/user")
    @ResponseBody
    public User addUser( @Validated @RequestBody User userDto) {
        return authService.addUser(userDto);
    }


    @RequestMapping(method=RequestMethod.DELETE, path="/user")
    public void deleteUser( @RequestParam(name = "id") int id ) {
         authService.deleteUser(id);
    }

    @GetMapping("/user/validate")
    public boolean validaateUser( @RequestParam(name = "name") String  name,@RequestParam(name = "password") String  password ) {
        return authService.validateUser(name,password);
    }
}
