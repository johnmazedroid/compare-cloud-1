package com.cloud.authService.service;

import com.cloud.authService.dao.UserDaoImpl;
import com.cloud.authService.model.User;
import com.cloud.authService.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AuthService {

    @Autowired
    UserDaoImpl userDao;



    public List<User> getUsers() {
        return userDao.findAll();
    }

    public User getUser(int id) {
        return userDao.findUserById(id);
    }

    public User addUser(User user) {
      user.setActive(true);
      user.setCreatedDate(new Date());
      user.setCreatedBy(Constant.ADMIN);
      userDao.saveUser(user);
      return user;
    }



    public void deleteUser(int id) {
        userDao.deleteById(id);
    }

    public boolean validateUser(String name,String password) {
        return userDao.validateUser(name,password);
    }
}
