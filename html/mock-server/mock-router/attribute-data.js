const express = require('express');
const InstanceTypeMockData = require('./data/mock-instance-type');
const OperatingSystemMockData = require('./data/mock-operating-system');
const InstanceFamilyMockData = require('./data/mock-instance-family');
const RegionMockData = require('./data/mock-region')

const DataByAttributeNameMockRouter = express.Router();

DataByAttributeNameMockRouter.get('/cloud-api/data/getUniqueNameBycp',
  (req, res) => {
  console.log('============================= GET DATA BY ATTRIBUTE NAME - '+ req.query.attribute + ' ==================================');
  console.log('request method :::', req.method);
  console.log('request url :::', req.url);
  console.log('request params :::', req.params);
  console.log('request params :::', req.query);
  console.log('request params :::', req.body);
  console.log('============================= GET DATA BY ATTRIBUTE NAME ==================================');
  if(req.query.attribute === 'instanceType') {
    res.send(InstanceTypeMockData);
  } else if(req.query.attribute === 'operatingSystem') {
    res.send(OperatingSystemMockData);
  } else if(req.query.attribute === 'instanceFamily') {
    res.send(InstanceFamilyMockData);
  } else if(req.query.attribute === 'region') {
    res.send(RegionMockData);
  }
});

module.exports = DataByAttributeNameMockRouter;
