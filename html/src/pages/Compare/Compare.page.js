import React, { useState, useRef } from 'react';
import { useForm } from 'react-hook-form';
import { Tab, Button, Form } from 'react-bootstrap';
import { isEmpty } from '@maze/utilities/Object.utilities';
import Stepper from '@maze/components/Stepper';

// Components
import FieldArray from './FieldArray';
import ModifyWorkload from '@maze/components/ModifyWorkload/ModifyWorkload.component';
import CompareOptimize from '@maze/components/CompareOptimize/CompareOptimize.component';

// import scss
import './Compare.page.scss';

// For Translation
import i18next from 'i18next';

const Compare = () => {
  const workLoadFormRef = useRef(); // Form Reference
  const defaultPage = 0;
  const totalPages = 2;
  const defaultValues = {
    workload: [
      {
        cloudProviders: [],
        tcoTerm: 3,
        computeResource: [],
      },
    ],
  };

  const [page, setPage] = useState(defaultPage);
  const [stateCloudProviders, setStateCloudProviders] = useState([]);
  const [stateMsgCreateNewWorkload, setStateMsgCreateNewWorkload] = useState(
    ''
  );
  const [stateWorkLoadFormData, setStateWorkLoadFormData] = useState(
    defaultValues
  );
  const [stateOptimizedData, setStateOptimizedData] = useState([]);

  const {
    control,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: 'all',
    defaultValues,
  });

  const onSubmit = (data) => setStateWorkLoadFormData(data);

  const onClickNext = () => {
    // setPage(page + 1); // For testing - to be removed
    const hasComputeResource = control.fieldArrayDefaultValuesRef.current.workload.some(
      (wl) => wl.computeResource.length
    );
    if (hasComputeResource) {
      setStateMsgCreateNewWorkload('');
      if (page === 0) {
        workLoadFormRef.current.dispatchEvent(
          new Event('submit', { cancelable: true, bubbles: true })
        );
        setTimeout(() => {
          if (isEmpty(errors)) {
            setPage(page + 1);
          }
        });
      } else {
        setPage(page + 1);
      }
    } else {
      setStateMsgCreateNewWorkload(i18next.t('defineComputeResources'));
    }
  };

  const getCloudProviders = (cloudProviders) => {
    setStateCloudProviders(cloudProviders);
  };

  const getOptimizedData = (optimizedData) => {
    setStateOptimizedData(optimizedData);
  };

  return (
    <div className={'page-compare'}>
      <Tab.Container
        className="maze-custom-tabs"
        defaultActiveKey={defaultPage}
        activeKey={page}
      >
        <div className={'p-0 m-0 hight-100 maze-custom-tabs'}>
          <div className="px-0">
            <Stepper
              steps={[
                {
                  title: i18next.t('titleDefineWorkLoads'),
                  onClick: () => {
                    setPage(0);
                  },
                },
                {
                  title: i18next.t('titleModifyWorkLoad'),
                  onClick: () => {
                    setPage(1);
                  },
                },
                {
                  title: i18next.t('titleCompareAndOptimize'),
                },
              ]}
              activeStep={page}
            />
          </div>
          <div className={'border p-0 bg-white'}>
            <Tab.Content>
              <Tab.Pane eventKey={0}>
                <>
                  <Form ref={workLoadFormRef} onSubmit={handleSubmit(onSubmit)}>
                    <FieldArray
                      {...{
                        control,
                        register,
                        errors,
                        getCloudProviders,
                        stateMsgCreateNewWorkload,
                      }}
                    />
                  </Form>
                </>
              </Tab.Pane>
              {/* TODO: Godwin should optimize here - POST call should be in common(or should be in parent) place, 
                not inside child get back to parent */}
              <Tab.Pane eventKey={1}>
                <>
                  {page === 1 && (
                    <ModifyWorkload
                      cloudProviders={stateCloudProviders}
                      definedData={stateWorkLoadFormData}
                      getOptimizedData={getOptimizedData}
                    />
                  )}
                </>
              </Tab.Pane>
              <Tab.Pane eventKey={2}>
                <>
                  <div className={'p-3'}>
                    {page === 2 && (
                      <CompareOptimize
                        cloudProviders={stateCloudProviders}
                        definedData={stateWorkLoadFormData}
                        optimizedData={stateOptimizedData}
                      ></CompareOptimize>
                    )}
                  </div>
                </>
              </Tab.Pane>
              {/* TODO: Godwin should optimize here */}
            </Tab.Content>
            <div className={'p-3 border-top text-right'}>
              {/* Footer Section */}
              {page !== 0 && (
                <Button
                  variant="light"
                  onClick={() => {
                    setPage(page - 1);
                  }}
                >
                  {i18next.t('common:previous')}
                </Button>
              )}
              {page !== totalPages && (
                <Button
                  variant="primary"
                  className={'ml-3'}
                  onClick={() => onClickNext()}
                >
                  {i18next.t('common:next')}
                </Button>
              )}
            </div>
          </div>
        </div>
      </Tab.Container>
    </div>
  );
};

export default Compare;
