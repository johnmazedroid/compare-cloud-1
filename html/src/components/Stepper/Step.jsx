import React from 'react';
import { PropTypes } from 'prop-types';
import classNames from 'classnames';
import './stepper.scss';

const Step = (props) => {
  const {
    title,
    icon,
    index,
    active,
    completed,
    // first,
    // isLast,
    onClick,
  } = props;

  const stepClass = classNames(
    'step',
    { 'bg-primary text-white': active },
    { 'bg-info text-white': completed },
    { 'bg-not-visited text-muted': !active && !completed }
  );

  const stepContent = icon ? <img src={icon} alt={index + 1} /> : index + 1;

  return (
    <div className={stepClass}>
      <div className="content">
        <div
          onClick={active || completed ? onClick : null}
          className="step-text"
        >
          <div className="circle">
            <span>{stepContent}</span>
          </div>
          <span className="title">{title}</span>
        </div>
      </div>
      {/* {!first && <div style={leftStyle}></div>}
      {!isLast && <div style={rightStyle}></div>} */}
    </div>
  );
};

Step.propTypes = {
  title: PropTypes.string,
  index: PropTypes.number,
  active: PropTypes.bool,
  completed: PropTypes.bool,
  first: PropTypes.bool,
  isLast: PropTypes.bool,
};

export default Step;
