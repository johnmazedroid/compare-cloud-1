import React, { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';

// Language
import i18next from 'i18next';

const SectionAlternateVM = (props) => {
  const {
    selectedLeftSection,
    filterDTO,
    data,
    getAlternateVMOptimizedValues,
  } = props;
  const [stateSelectedVMData, setStateSelectedVMData] = useState([]);
  useEffect(() => {
    const sortedData = data
      .map((item) => {
        return {
          ...item,
          serverPricing: item.serverPricing.sort(
            (a, b) => a.hourlyPrice - b.hourlyPrice
          ),
        };
      })
      .sort((a, b) => a.totalPrice - b.totalPrice)
      .filter(
        (item) =>
          item.serverAttributes.cloudProvider.name ===
          selectedLeftSection.selectedItem
      );
    setStateSelectedVMData(sortedData);
  }, [selectedLeftSection, filterDTO, data]);

  useEffect(() => {
    getAlternateVMOptimizedValues(stateSelectedVMData);
  }, [getAlternateVMOptimizedValues, stateSelectedVMData]);

  return (
    <>
      <div className={'py-3 border-bottom'}>
        <h5>{i18next.t('alternateVMOptions')}</h5>
        <Table striped hover size="sm">
          <thead>
            <tr>
              <th>{i18next.t('instanceName')}</th>
              <th>{i18next.t('vCPU')}</th>
              <th>{i18next.t('memory')}</th>
              <th>{i18next.t('processingSpeed')}</th>
              <th>{i18next.t('cost')}</th>
              <th>{i18next.t('replaceFor')}</th>
            </tr>
          </thead>
          <tbody>
            {stateSelectedVMData.length === 0 && (
              <tr>
                <td colSpan={6}>{i18next.t('common:noDataFound')}</td>
              </tr>
            )}
            {stateSelectedVMData &&
              stateSelectedVMData.length > 0 &&
              stateSelectedVMData.map((VM, index) => {
                return VM.serverPricing.map((sp, spIndex) => {
                  return (
                    <tr
                      key={spIndex}
                      className={index === 0 && spIndex === 0 ? 'd-none' : ''}
                    >
                      <td>{VM.serverAttributes.instance.instanceName}</td>
                      <td>{VM.serverAttributes.virtualCpu}</td>
                      <td>{VM.serverAttributes.memory}</td>
                      <td>
                        {VM.serverAttributes.processor.processorFamilyMapped}
                      </td>
                      <td>
                        {sp.price} / {sp.hourlyPrice}
                      </td>
                      <td>NA</td>
                    </tr>
                  );
                });
              })}
          </tbody>
        </Table>
      </div>
    </>
  );
};

export default SectionAlternateVM;
