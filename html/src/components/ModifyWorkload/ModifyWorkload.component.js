import React, { useState, useEffect, useRef } from 'react';
import { Row, Col, Form } from 'react-bootstrap';
import { errorMessage } from '@maze/utilities/Error.utilities';
import { toast } from 'react-toastify';

// Services
import { postComputeResourceHTTPService } from '@maze/http-post-services/compute-resource-http.services';

// Language
import i18next from 'i18next';

// Components
import ListItems from '@maze/shared-components/ListItems/ListItems.component';
import SectionWorkload from './SectionWorkload/SectionWorkload.component';
import SectionSelectedVM from './SectionSelectedVM/SectionSelectedVM.component';
import SectionAlternateVM from './SectionAlternateVM/SectionAlternateVM.component';

// SCSS
import './ModifyWorkload.component.scss';

const ModifyWorkload = (props) => {
  const { cloudProviders, definedData, getOptimizedData } = props;
  const modifyWorkloadFormRef = useRef();
  const [stateCloudProviders, setStateCloudProviders] = useState([]);

  const [stateMenuItems, setStateMenuItems] = useState([]);
  const [stateWorkload, setStateWorkload] = useState([]);
  const [stateSelectedLeftSection, setStateSelectedLeftSection] = useState([]);
  const [stateFilterDTO, setStateFilterDTO] = useState([]);
  const [stateComputeResourceData, setStateComputeResourceData] = useState([]);

  useEffect(() => {
    let unmounted = false;
    const { workload } = definedData;
    const cloudProvidersName = cloudProviders
      .filter((cp) => workload[0].cloudProviders.includes(cp.cloudProviderId))
      .map((cp) => ({ name: cp.name, value: cp.cloudProviderId }));
    setStateCloudProviders(cloudProviders);
    setStateWorkload(workload);

    if (cloudProvidersName.length > 0) {
      const menuItems = cloudProvidersName.map((item) => {
        return {
          name: item.name,
          value: item.value,
          className: 'parent',
          children: [
            {
              name: i18next.t('headerComputeResources'),
              className: 'child',
            },
          ],
        };
      });
      setStateSelectedLeftSection({
        selectedItem: menuItems[0].name,
        selectedSection: menuItems[0].children[0].name,
      });
      setStateMenuItems(menuItems);
    }

    const transformDefinedWorkload = workload.map((wl) => {
      return wl.computeResource.map((cp) => {
        return {
          ...cp,
          tcoTerm: wl.tcoTerm.toString(),
          cloudProvider: stateCloudProviders.map((cp) => cp.name),
          leaseTerm: cp.payAsYouGo ? 'On Demand' : 'Reserved',
          usagePerMonth: cp.payAsYouGo ? '730' : cp.usagePerMonth,
          payAsYouGo: cp.payAsYouGo.toString(),
        };
      });
    })[0];

    const definedWorkloadBackendDTO = transformDefinedWorkload.map((twl) => {
      const workloadKeys = Object.keys(twl).filter(
        (key) => !['leaseTerm', 'payAsYouGo'].includes(key)
      );
      return {
        workload: workloadKeys.map((wlKey) => {
          const value = Array.isArray(twl[wlKey]) ? twl[wlKey] : [twl[wlKey]];
          return {
            key: wlKey,
            value,
            search: value.length > 1 ? 'CONTAINS' : 'EQUALITY',
          };
        }),
      };
    });
    setStateFilterDTO(definedWorkloadBackendDTO);

    const computeResourceData$ = postComputeResourceHTTPService(
      definedWorkloadBackendDTO
    );
    computeResourceData$
      .then((response) => {
        if (!unmounted) {
          if (response.length) {
            setStateComputeResourceData(response);
          }
        }
      })
      .catch((error) => {
        toast.error(`${errorMessage(error)}`);
      });

    return () => {
      unmounted = true;
    };
  }, [cloudProviders, definedData, stateCloudProviders]);

  const onSelectMenuItem = (selectedItem) => {
    setStateSelectedLeftSection(selectedItem);
  };

  const getAlternateVMOptimizedValues = (optimizedValue) => {
    getOptimizedData(optimizedValue);
  };

  return (
    <>
      <div className={'modify-workload-outer-box'}>
        <Row className={'m-0 h-100'}>
          <Col xs={3} className={'p-0 m-0 border-right'}>
            <ListItems items={stateMenuItems} {...{ onSelectMenuItem }} />
          </Col>
          <Col xs={9} className={'py-3'}>
            <Form ref={modifyWorkloadFormRef}>
              <SectionWorkload
                data={stateWorkload}
                cloudProviders={stateCloudProviders}
                selectedLeftSection={stateSelectedLeftSection}
              />
              <SectionSelectedVM
                selectedLeftSection={stateSelectedLeftSection}
                filterDTO={stateFilterDTO}
                workload={stateWorkload}
                data={stateComputeResourceData}
              />
              <SectionAlternateVM
                selectedLeftSection={stateSelectedLeftSection}
                filterDTO={stateFilterDTO}
                workload={stateWorkload}
                data={stateComputeResourceData}
                getAlternateVMOptimizedValues={getAlternateVMOptimizedValues}
              />
            </Form>
          </Col>
        </Row>
      </div>
    </>
  );
};

export default ModifyWorkload;
