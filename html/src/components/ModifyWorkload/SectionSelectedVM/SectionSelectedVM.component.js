import React, { useEffect, useState } from 'react';
import { Table } from 'react-bootstrap';

// Language
import i18next from 'i18next';

const SectionSelectedVM = (props) => {
  const { selectedLeftSection, filterDTO, data, workload } = props;
  const [stateSelectedVMData, setStateSelectedVMData] = useState([]);
  const [stateWorkload, setStateWorkload] = useState([]);

  useEffect(() => {
    setStateWorkload(workload);
    const sortedData = data
      .map((item) => {
        return {
          ...item,
          serverPricing: item.serverPricing.sort(
            (a, b) => a.hourlyPrice - b.hourlyPrice
          ),
        };
      })
      .sort((a, b) => a.totalPrice - b.totalPrice)
      .filter(
        (item) =>
          item.serverAttributes.cloudProvider.name ===
          selectedLeftSection.selectedItem
      );
    setStateSelectedVMData(sortedData);
  }, [selectedLeftSection, filterDTO, data, workload]);

  return (
    <>
      <div className={'py-3 border-bottom'}>
        <h5>{i18next.t('selectedVMInstances')}</h5>
        <Table striped hover size="sm">
          <thead>
            <tr>
              <th>#</th>
              <th>{i18next.t('instanceName')}</th>
              <th>{i18next.t('noOfServers')}</th>
              <th>{i18next.t('cost')}</th>
            </tr>
          </thead>
          <tbody>
            {stateSelectedVMData.length === 0 && (
              <tr>
                <td colSpan={4}>{i18next.t('common:noDataFound')}</td>
              </tr>
            )}
            {stateSelectedVMData.length > 0 && (
              <tr>
                <td>1</td>
                <td>
                  {
                    stateSelectedVMData[0].serverAttributes.instance
                      .instanceName
                  }
                </td>
                <td>{stateWorkload[0].computeResource[0].noOfServers}</td>
                <td>
                  {stateSelectedVMData[0].totalPrice} /{' '}
                  {stateSelectedVMData[0].serverPricing[0].hourlyPrice}
                </td>
              </tr>
            )}
          </tbody>
        </Table>
      </div>
    </>
  );
};

export default SectionSelectedVM;
