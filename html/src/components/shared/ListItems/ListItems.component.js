import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { ChevronDown } from "react-bootstrap-icons";

// SCSS
import "./ListItems.component.scss";

const ListItems = (props) => {
  const { items, onSelectMenuItem } = props;
  const [stateActive, setStateActive] = useState({});

  useEffect(() => {
    if(items.length) {
      setStateActive({
        selectedItem: items[0].name,
        selectedSection: items[0].children[0].name
      })
    }    
  }, [items])

  const onClickListItem = (event) => {
    const selectedMenuItem = {
      selectedItem: event.currentTarget.getAttribute("data-value"),
      selectedSection: event.currentTarget.getAttribute("data-section"),
    };
    onSelectMenuItem(selectedMenuItem);
    setStateActive(selectedMenuItem);
  };

  return (
    <>
      <ul className={"ul-lists-items m-0 p-0"}>
        {items.map((item, listItemIndex) => {
          return (
            <React.Fragment key={listItemIndex}>
              <li
                key={`parent-${listItemIndex}`}
                className={`border-bottom cursor-pointer px-3 py-1 ${item.className}`}
              >
                <ChevronDown key={`icon-${listItemIndex}`} size={12} />{" "}
                <span key={`name-${listItemIndex}`}>{item.name}</span>
              </li>
              {item.children.length &&
                item.children.map((child, childIndex) => {
                  return (
                    <li
                      key={`child-${childIndex}`}
                      className={`border-bottom cursor-pointer px-3 py-1 ${
                        child.className
                      } ${
                        stateActive.selectedItem === item.name &&
                        stateActive.selectedSection === child.name
                          ? "active"
                          : ""
                      }`}
                      data-value={item.name}
                      data-section={child.name}
                      onClick={onClickListItem}
                    >
                      <span key={`child-name-${childIndex}`}>{child.name}</span>
                    </li>
                  );
                })}
            </React.Fragment>
          );
        })}
      </ul>
    </>
  );
};

ListItems.propTypes = {
  items: PropTypes.array.isRequired,
};

export default ListItems;
