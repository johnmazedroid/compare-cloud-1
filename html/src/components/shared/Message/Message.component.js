import React from 'react';
import PropTypes from 'prop-types';
import { XCircleFill } from 'react-bootstrap-icons';

// For Translation
import i18next from 'i18next';

const Message = (props) => {
  const { data } = props;
  return (
    <>
      {data && (
        <div className="text-danger">
          <XCircleFill />
          <span className={'ml-1'}>
            {data.message ? data.message : i18next.t('common:inputRequired')}
          </span>
        </div>
      )}
    </>
  );
};

Message.propTypes = {
  data: PropTypes.object,
};

export default Message;
