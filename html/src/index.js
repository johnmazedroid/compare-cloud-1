// Libraries
import React from "react";
import ReactDOM from "react-dom";
import moment from "moment";
import { BrowserRouter } from "react-router-dom";
import reportWebVitals from "./reportWebVitals";

// import Amplify from 'aws-amplify';
// import awsConfig from './aws-exports';

// Components, pages
import App from "./App";

// Css, Scss
import "./index.scss";

// translation configuration
import "./i18n";

// Amplify.configure(awsConfig);

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("appCloudCompare")
);

module.hot.accept();
reportWebVitals();
