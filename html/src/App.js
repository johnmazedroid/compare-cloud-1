// Libraries
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import { ToastContainer } from 'react-toastify';

// Components, pages and views
import Header from './components/Header/Header.component';
import Home from './pages/Home/Home.page';
import Compare from './pages/Compare/Compare.page';
import About from './pages/About/About.page';

// Css, SCSS
import './App.scss';

function App() {
  return (
    <div className="cloud-compare-app h-100 gray-400">
        <Header />
        <Container className={'pt-85 content-container h-100'}>
          <Switch>    
            <Route path='/' component={Home} exact />            
            <Route path='/compare' component={Compare} exact />
            <Route path='/services' component={About} exact />
          </Switch>
        </Container>
        <ToastContainer />
    </div>
  );
}

export default App;
