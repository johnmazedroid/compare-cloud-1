import React, { ReactElement } from 'react';
import { Layout } from 'antd';
import classNames from 'classnames/bind';

import './layout-wrapper.module.scss';

export interface LayoutWrapperProps {
  children: ReactElement;
  bgTransparent?: boolean;
  customClassNames?: string;
  title?: string;
}

export const LayoutWrapper = (props: LayoutWrapperProps) => {
  const classnames = classNames(props.customClassNames, {
    'bg-transparent': props.bgTransparent,
  });

  return (
    <Layout className={classnames}>
      {props.title && <h5 className={'py-2 px-3 m-0'}>{props.title}</h5>}
      {props.children}
    </Layout>
  );
}
