// Expect Array, key and value to match
export const removeObjectFromArray = (arrayIn: any[], identiferKey: string, identiferValue: any): boolean => {
    if(Array.isArray(arrayIn)) {
        const removeIndex = arrayIn.map((item) => item[identiferKey]).indexOf(identiferValue);

        if(removeIndex > -1) {
            arrayIn.splice(removeIndex, 1);        
            return true;
        }
    }    

    return false;
}

export const insertObjectToArray = (arrayIn: any[], replaceIndex: number, replacerObject: any): void => {
    if(Array.isArray(arrayIn)) {
        arrayIn.splice(replaceIndex, 0, replacerObject);
    }
}