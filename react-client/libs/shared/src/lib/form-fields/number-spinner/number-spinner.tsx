import { InputNumber, InputNumberProps } from 'antd';
import React, { ReactElement } from 'react';

import './number-spinner.module.scss';

export interface NumberSpinnerProps extends InputNumberProps {
  customProperties?: {
    showLabel?: boolean;
    label?: string
  }
}

export const NumberSpinner = (props: NumberSpinnerProps): ReactElement => {
  const getDefaultProperties = (propsIn: NumberSpinnerProps): InputNumberProps => {
    const defaultProperties = {
      ...propsIn
    }
    delete defaultProperties.customProperties; // Delete Custom Properties and make sure to default flow works fine
    return {
      ...defaultProperties
    }
  }

  const defaultProperties: InputNumberProps = getDefaultProperties(props);
  
  return (
    <>
      {props.customProperties.showLabel && <label className="m-0">{props.customProperties.label}</label>}
      <InputNumber {...defaultProperties} />
    </>
  );
}
