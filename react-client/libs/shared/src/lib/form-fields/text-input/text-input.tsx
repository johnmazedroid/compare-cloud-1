import React from 'react';
import { Input, InputProps } from 'antd';

import './text-input.module.scss';

export interface TextInput extends InputProps {  
}

export const TextInput = (props: TextInput) => {
  return (
    <Input {...props} />
  );
}
