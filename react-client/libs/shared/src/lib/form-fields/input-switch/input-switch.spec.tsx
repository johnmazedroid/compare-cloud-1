import React from 'react';
import { render } from '@testing-library/react';

import {InputSwitch} from './input-switch';

describe('InputSwitch', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<InputSwitch />);
    expect(baseElement).toBeTruthy();
  });
});
