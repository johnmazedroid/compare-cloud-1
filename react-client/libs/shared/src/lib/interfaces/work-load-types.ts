export interface ComputeResourceType {
  workloadName: string;
  estimatedHoursPerMonth: number;
  instanceType: string;
  operatingSystem: string;
  instanceFamily: string;
  region: string;
  noOfServers: number;
  vCPUsPerServer: number;
  memoryPerServer: number;
  payAsYouGo: boolean;
  uniqueIdentifier?: string;
}

export interface WorkLoadObjectDTO {
  cloudProviders: string[];
  tcoTerm: number;  
  computeResource: ComputeResourceType[]; 
}