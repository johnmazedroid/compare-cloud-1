export interface TextInputInterface {
    placeholder?: string;
    onChange?: () => void;
    allowClear?: boolean;
    disabled?: boolean;
    defaultValue?: string;
    maxLength?: number;
  }