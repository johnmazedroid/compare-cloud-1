import * as express from 'express';
import { Message } from '@react-client/api-interfaces';

import CloudProvidersMockRouter from './mock-services/cloud-providers';
import DataByAttributeNameMockRouter from './mock-services/attribute-data';
import ComputeResourcePostMockRouter from './mock-services/post-compute-resource';

const app = express();

const greeting: Message = { message: 'Welcome to api!' };

app.get('/api', (req, res) => {
  res.send(greeting);
});

app.use('', CloudProvidersMockRouter);
app.use('', DataByAttributeNameMockRouter);
app.use('', ComputeResourcePostMockRouter);

const port = process.env.port || 3333;
const server = app.listen(port, () => {
  console.log('Listening at http://localhost:' + port);
});
server.on('error', console.error);
