export const ComputeResourceResponseMockData = [{
  "serverAttributes": {
    "serverAttributesId": 1848,
    "virtualCpu": 36,
    "memory": 72,
    "defaultStorage": 0,
    "cloudProvider": {
      "cloudProviderId": "1",
      "name": "AWS",
      "description": "Amazon Web Services",
      "displayOrder": 1,
      "hibernateLazyInitializer": {}
    },
    "operatingSystem": {
      "operatingSystemMstId": 2,
      "operatingSystem": "linux",
      "operatingSystemMapped": "Linux",
      "operatingSystemFamily": "Linux",
      "cloudProvider": {
        "cloudProviderId": "1",
        "name": "AWS",
        "description": "Amazon Web Services",
        "displayOrder": 1,
        "hibernateLazyInitializer": {}
      },
      "hibernateLazyInitializer": {}
    },
    "region": {
      "regionMstId": 13,
      "region": "ap-northeast-1",
      "regionGroupMapped": "Asia Pacific",
      "locationMapped": "Tokyo",
      "cloudProvider": {
        "cloudProviderId": "1",
        "name": "AWS",
        "description": "Amazon Web Services",
        "displayOrder": 1,
        "hibernateLazyInitializer": {}
      },
      "hibernateLazyInitializer": {}
    },
    "instance": {
      "instanceMstId": 529,
      "instanceName": "c5.9xlarge",
      "cloudProvider": {
        "cloudProviderId": "1",
        "name": "AWS",
        "description": "Amazon Web Services",
        "displayOrder": 1,
        "hibernateLazyInitializer": {}
      },
      "hibernateLazyInitializer": {}
    },
    "instanceType": {
      "instanceTypeMstId": 6,
      "instanceType": "Virtual",
      "instanceTypeMapped": "Virtual",
      "cloudProvider": {
        "cloudProviderId": "1",
        "name": "AWS",
        "description": "Amazon Web Services",
        "displayOrder": 1,
        "hibernateLazyInitializer": {}
      },
      "hibernateLazyInitializer": {}
    },
    "instanceFamily": {
      "instanceFamilyMstId": 8,
      "instanceFamily": "Compute optimized",
      "instanceFamilyMapped": "Compute Optimized",
      "cloudProvider": {
        "cloudProviderId": "1",
        "name": "AWS",
        "description": "Amazon Web Services",
        "displayOrder": 1,
        "hibernateLazyInitializer": {}
      },
      "hibernateLazyInitializer": {}
    },
    "processor": {
      "processorMstId": 33,
      "processor": "Intel Xeon Platinum 8124M 3 GHz",
      "processorFamilyMapped": "Intel",
      "cloudProvider": {
        "cloudProviderId": "1",
        "name": "AWS",
        "description": "Amazon Web Services",
        "displayOrder": 1,
        "hibernateLazyInitializer": {}
      },
      "hibernateLazyInitializer": {}
    }
  },
  "serverPricing": [
    {
      "serverPricingId": 23991,
      "hourlyPrice": 1.48596,
      "leaseTerm": {
        "leaseTermMstId": 10,
        "leaseTerm": "1yr",
        "leaseTermMapped": "1yr",
        "cloudProvider": {
          "cloudProviderId": "1",
          "name": "AWS",
          "description": "Amazon Web Services",
          "displayOrder": 1,
          "hibernateLazyInitializer": {}
        },
        "hibernateLazyInitializer": {}
      },
      "offeringClass": {
        "offeringClassMstId": 7,
        "offeringClass": "Convertible",
        "offeringClassMapped": "Convertible",
        "cloudProvider": {
          "cloudProviderId": "1",
          "name": "AWS",
          "description": "Amazon Web Services",
          "displayOrder": 1,
          "hibernateLazyInitializer": {}
        },
        "hibernateLazyInitializer": {}
      },
      "paymentOption": {
        "paymentOptionMstId": 8,
        "paymentOption": "All Upfront",
        "paymentOptionMapped": "All Upfront",
        "cloudProvider": {
          "cloudProviderId": "1",
          "name": "AWS",
          "description": "Amazon Web Services",
          "displayOrder": 1,
          "hibernateLazyInitializer": {}
        },
        "hibernateLazyInitializer": {}
      },
      "serverAttributesId": 1848,
      "price": 12838.695
    },
    {
      "serverPricingId": 23993,
      "hourlyPrice": 1.13253,
      "leaseTerm": {
        "leaseTermMstId": 10,
        "leaseTerm": "1yr",
        "leaseTermMapped": "1yr",
        "cloudProvider": {
          "cloudProviderId": "1",
          "name": "AWS",
          "description": "Amazon Web Services",
          "displayOrder": 1,
          "hibernateLazyInitializer": {}
        },
        "hibernateLazyInitializer": {}
      },
      "offeringClass": {
        "offeringClassMstId": 8,
        "offeringClass": "Standard",
        "offeringClassMapped": "Standard",
        "cloudProvider": {
          "cloudProviderId": "1",
          "name": "AWS",
          "description": "Amazon Web Services",
          "displayOrder": 1,
          "hibernateLazyInitializer": {}
        },
        "hibernateLazyInitializer": {}
      },
      "paymentOption": {
        "paymentOptionMstId": 8,
        "paymentOption": "All Upfront",
        "paymentOptionMapped": "All Upfront",
        "cloudProvider": {
          "cloudProviderId": "1",
          "name": "AWS",
          "description": "Amazon Web Services",
          "displayOrder": 1,
          "hibernateLazyInitializer": {}
        },
        "hibernateLazyInitializer": {}
      },
      "serverAttributesId": 1848,
      "price": 9785.059
    }
  ],
  "computeResourceInputVO": {
    "workload": [
      {
        "value": [
          "AWS"
        ],
        "search": "EQUALITY",
        "key": "cloudProvider"
      },
      {
        "value": [
          "Tokyo"
        ],
        "search": "EQUALITY",
        "key": "region"
      },
      {
        "value": [
          "Virtual"
        ],
        "search": "EQUALITY",
        "key": "instanceType"
      },
      {
        "value": [
          "Compute Optimized"
        ],
        "search": "EQUALITY",
        "key": "instanceFamily"
      },
      {
        "value": [
          "Linux"
        ],
        "search": "EQUALITY",
        "key": "operatingSystem"
      },
      {
        "value": [
          "36"
        ],
        "search": "EQUALITY",
        "key": "noOfServers"
      },
      {
        "value": [
          "72"
        ],
        "search": "EQUALITY",
        "key": "memory"
      },
      {
        "value": [
          "36"
        ],
        "search": "EQUALITY",
        "key": "virtualCpu"
      },
      {
        "value": [
          "1yr"
        ],
        "search": "EQUALITY",
        "key": "leaseTerm"
      },
      {
        "value": [
          "All Upfront"
        ],
        "search": "EQUALITY",
        "key": "paymentOption"
      }
    ]
  },
  "totalPrice": 9785.059
},
  {
    "serverAttributes": {
      "serverAttributesId": 1924,
      "virtualCpu": 36,
      "memory": 72,
      "defaultStorage": 1,
      "cloudProvider": {
        "cloudProviderId": "1",
        "name": "AWS",
        "description": "Amazon Web Services",
        "displayOrder": 1,
        "hibernateLazyInitializer": {}
      },
      "operatingSystem": {
        "operatingSystemMstId": 2,
        "operatingSystem": "linux",
        "operatingSystemMapped": "Linux",
        "operatingSystemFamily": "Linux",
        "cloudProvider": {
          "cloudProviderId": "1",
          "name": "AWS",
          "description": "Amazon Web Services",
          "displayOrder": 1,
          "hibernateLazyInitializer": {}
        },
        "hibernateLazyInitializer": {}
      },
      "region": {
        "regionMstId": 13,
        "region": "ap-northeast-1",
        "regionGroupMapped": "Asia Pacific",
        "locationMapped": "Tokyo",
        "cloudProvider": {
          "cloudProviderId": "1",
          "name": "AWS",
          "description": "Amazon Web Services",
          "displayOrder": 1,
          "hibernateLazyInitializer": {}
        },
        "hibernateLazyInitializer": {}
      },
      "instance": {
        "instanceMstId": 554,
        "instanceName": "c5d.9xlarge",
        "cloudProvider": {
          "cloudProviderId": "1",
          "name": "AWS",
          "description": "Amazon Web Services",
          "displayOrder": 1,
          "hibernateLazyInitializer": {}
        },
        "hibernateLazyInitializer": {}
      },
      "instanceType": {
        "instanceTypeMstId": 6,
        "instanceType": "Virtual",
        "instanceTypeMapped": "Virtual",
        "cloudProvider": {
          "cloudProviderId": "1",
          "name": "AWS",
          "description": "Amazon Web Services",
          "displayOrder": 1,
          "hibernateLazyInitializer": {}
        },
        "hibernateLazyInitializer": {}
      },
      "instanceFamily": {
        "instanceFamilyMstId": 8,
        "instanceFamily": "Compute optimized",
        "instanceFamilyMapped": "Compute Optimized",
        "cloudProvider": {
          "cloudProviderId": "1",
          "name": "AWS",
          "description": "Amazon Web Services",
          "displayOrder": 1,
          "hibernateLazyInitializer": {}
        },
        "hibernateLazyInitializer": {}
      },
      "processor": {
        "processorMstId": 33,
        "processor": "Intel Xeon Platinum 8124M 3 GHz",
        "processorFamilyMapped": "Intel",
        "cloudProvider": {
          "cloudProviderId": "1",
          "name": "AWS",
          "description": "Amazon Web Services",
          "displayOrder": 1,
          "hibernateLazyInitializer": {}
        },
        "hibernateLazyInitializer": {}
      }
    },
    "serverPricing": [
      {
        "serverPricingId": 24987,
        "hourlyPrice": 1.67648,
        "leaseTerm": {
          "leaseTermMstId": 10,
          "leaseTerm": "1yr",
          "leaseTermMapped": "1yr",
          "cloudProvider": {
            "cloudProviderId": "1",
            "name": "AWS",
            "description": "Amazon Web Services",
            "displayOrder": 1,
            "hibernateLazyInitializer": {}
          },
          "hibernateLazyInitializer": {}
        },
        "offeringClass": {
          "offeringClassMstId": 7,
          "offeringClass": "Convertible",
          "offeringClassMapped": "Convertible",
          "cloudProvider": {
            "cloudProviderId": "1",
            "name": "AWS",
            "description": "Amazon Web Services",
            "displayOrder": 1,
            "hibernateLazyInitializer": {}
          },
          "hibernateLazyInitializer": {}
        },
        "paymentOption": {
          "paymentOptionMstId": 8,
          "paymentOption": "All Upfront",
          "paymentOptionMapped": "All Upfront",
          "cloudProvider": {
            "cloudProviderId": "1",
            "name": "AWS",
            "description": "Amazon Web Services",
            "displayOrder": 1,
            "hibernateLazyInitializer": {}
          },
          "hibernateLazyInitializer": {}
        },
        "serverAttributesId": 1924,
        "price": 14484.787
      },
      {
        "serverPricingId": 24977,
        "hourlyPrice": 1.29121,
        "leaseTerm": {
          "leaseTermMstId": 10,
          "leaseTerm": "1yr",
          "leaseTermMapped": "1yr",
          "cloudProvider": {
            "cloudProviderId": "1",
            "name": "AWS",
            "description": "Amazon Web Services",
            "displayOrder": 1,
            "hibernateLazyInitializer": {}
          },
          "hibernateLazyInitializer": {}
        },
        "offeringClass": {
          "offeringClassMstId": 8,
          "offeringClass": "Standard",
          "offeringClassMapped": "Standard",
          "cloudProvider": {
            "cloudProviderId": "1",
            "name": "AWS",
            "description": "Amazon Web Services",
            "displayOrder": 1,
            "hibernateLazyInitializer": {}
          },
          "hibernateLazyInitializer": {}
        },
        "paymentOption": {
          "paymentOptionMstId": 8,
          "paymentOption": "All Upfront",
          "paymentOptionMapped": "All Upfront",
          "cloudProvider": {
            "cloudProviderId": "1",
            "name": "AWS",
            "description": "Amazon Web Services",
            "displayOrder": 1,
            "hibernateLazyInitializer": {}
          },
          "hibernateLazyInitializer": {}
        },
        "serverAttributesId": 1924,
        "price": 11156.055
      }
    ],
    "computeResourceInputVO": {
      "workload": [
        {
          "value": [
            "AWS"
          ],
          "search": "EQUALITY",
          "key": "cloudProvider"
        },
        {
          "value": [
            "Tokyo"
          ],
          "search": "EQUALITY",
          "key": "region"
        },
        {
          "value": [
            "Virtual"
          ],
          "search": "EQUALITY",
          "key": "instanceType"
        },
        {
          "value": [
            "Compute Optimized"
          ],
          "search": "EQUALITY",
          "key": "instanceFamily"
        },
        {
          "value": [
            "Linux"
          ],
          "search": "EQUALITY",
          "key": "operatingSystem"
        },
        {
          "value": [
            "36"
          ],
          "search": "EQUALITY",
          "key": "noOfServers"
        },
        {
          "value": [
            "72"
          ],
          "search": "EQUALITY",
          "key": "memory"
        },
        {
          "value": [
            "36"
          ],
          "search": "EQUALITY",
          "key": "virtualCpu"
        },
        {
          "value": [
            "1yr"
          ],
          "search": "EQUALITY",
          "key": "leaseTerm"
        },
        {
          "value": [
            "All Upfront"
          ],
          "search": "EQUALITY",
          "key": "paymentOption"
        }
      ]
    },
    "totalPrice": 11156.055
  }]
