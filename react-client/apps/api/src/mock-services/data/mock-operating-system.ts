export const OperatingSystemMockData = [{
    "name": "Linux",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "Red Hat Linux",
    "cloudProviderIdList": [
      "1",
      "2"
    ]
  },
  {
    "name": "SUSE",
    "cloudProviderIdList": [
      "1",
      "2"
    ]
  },
  {
    "name": "Windows",
    "cloudProviderIdList": [
      "1",
      "2"
    ]
  },
  {
    "name": "Ubuntu",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "NA",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "DEFAULT",
    "cloudProviderIdList": [
      "3"
    ]
  }];

