import React from 'react';
import { render } from '@testing-library/react';

import ComputeResource from './compute-resource';

describe('ComputeResource', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<ComputeResource />);
    expect(baseElement).toBeTruthy();
  });
});
