import { LayoutWrapper, CheckBoxGroup, NumberSpinner, WorkLoadObjectDTO, getDataByCloudProvider }  from '@react-client/shared';
import { Button, Col, Divider, Form, Row, Collapse } from 'antd';
import { CheckboxValueType } from 'antd/lib/checkbox/Group';
import i18next from 'i18next';
import React, { Component } from 'react';
import ComputeResource from '../compute-resource/compute-resource';

import './define-workload.scss';

export interface DefineWorkloadProps {
  data: WorkLoadObjectDTO;
  formInputData: any
}

export interface DefineWorkloadState {
  panelActiveKey: string[];
  formInputDefaultData: any;
}

class DefineWorkload extends Component<DefineWorkloadProps, DefineWorkloadState> {
  state: DefineWorkloadState = {
    panelActiveKey: ['1'],
    formInputDefaultData: null
  }

  updateComputeResourceData = (updatedData) => {
    console.log('updatedData ::', updatedData);
    this.props.data.computeResource = updatedData;
  }

  private onChangePanel = (event) => {
    const selectedActiveKey: string[] = event.filter(item => !this.state.panelActiveKey.includes(item));
    this.setState({
      panelActiveKey: selectedActiveKey
    })
  }

  componentDidUpdate = (prevProps) => {
    if(
      (prevProps.formInputData?.cloudProvidersOptions?.length !== this.props.formInputData?.cloudProvidersOptions.length) ||
      (prevProps.formInputData?.instanceTypesData?.length !== this.props.formInputData?.instanceTypesData.length) ||
      (prevProps.formInputData?.operatingSystemsData?.length !== this.props.formInputData?.operatingSystemsData.length) ||
      (prevProps.formInputData?.instanceFamiliesData?.length !== this.props.formInputData?.instanceFamiliesData.length) ||
      (prevProps.formInputData?.regionsData?.length !== this.props.formInputData?.regionsData.length) 
    ) {
      this.setState({
        formInputDefaultData: {
          ...this.props.formInputData
        }
      })
    }
  }

  private updateFormData = (cloudProviders: CheckboxValueType[]) => {
    const {
      instanceTypesData,
      operatingSystemsData,
      instanceFamiliesData,
      regionsData
    } = this.props.formInputData;

    this.setState({
      formInputDefaultData: {
        ...this.props.formInputData,
        instanceTypeOptions: getDataByCloudProvider(instanceTypesData, cloudProviders),
        operatingSystemOptions: getDataByCloudProvider(operatingSystemsData, cloudProviders),
        instanceFamilyOptions: getDataByCloudProvider(instanceFamiliesData, cloudProviders),
        regionOptions: getDataByCloudProvider(regionsData, cloudProviders)
      }
    })
  }

  render = () => {
    const { Panel } = Collapse;
    const {data, formInputData} = this.props;
    const {formInputDefaultData} = this.state;
    return (    
      <>
        <Form 
          name="form-workload" 
          autoComplete="off" 
          layout={'vertical'}>
            <LayoutWrapper bgTransparent customClassNames="m-3">
              <Row className="p-3 box-shadow-thin">
                <Col md={16} sm={24} xs={24}>
                  {formInputDefaultData && 
                    <CheckBoxGroup 
                      options={formInputDefaultData.cloudProvidersOptions} 
                      className='ml-2'
                      customProperties={{
                        showLabel: true,
                        label: i18next.t('labelCloudServiceProviders')
                      }}  
                      name="cloudProviders" 
                      defaultValue={data.cloudProviders}
                      onChange={(value) => {
                        data.cloudProviders = value.map(val => val.toString());
                        this.updateFormData(value);
                      }}
                      />
                    }
                </Col>
                <Col md={8} sm={24} xs={24} className={'text-md-right text-sm-left text-xs-left mt-sm-4 mt-md-0 mt-lg-0 mt-xl-0'}>
                      <NumberSpinner 
                        min={1} 
                        max={5} 
                        size={'small'} 
                        className={'ml-2'} 
                        customProperties={{
                          showLabel: true,
                          label: i18next.t('labelTCOTermYear')
                        }}
                        name="tcoTerm"
                        defaultValue={data.tcoTerm}
                        onChange={(value) => {
                          data.tcoTerm = parseInt(value.toString());
                        }}
                      />
                </Col>
              </Row>
            </LayoutWrapper>

            <LayoutWrapper bgTransparent customClassNames="m-3">
              <Collapse activeKey={this.state.panelActiveKey} defaultActiveKey={['1']} onChange={this.onChangePanel}>
                <Panel header={i18next.t('headerComputeResources')} key="1">
                  <ComputeResource 
                    formData={data}  
                    formFieldData={formInputDefaultData}
                    getComputeResourceData={this.updateComputeResourceData} />
                </Panel>
                <Panel header={i18next.t('storage')} key="2">
                  <div className="p-4">{i18next.t('storage')}</div>
                </Panel>
                <Panel header={i18next.t('database')} key="3">
                  <div className="p-4">{i18next.t('database')}</div>
                </Panel>
              </Collapse>
            </LayoutWrapper>
        </Form>
      </>
  );
  } 
}

export default DefineWorkload;

// const DefineWorkload = (props: DefineWorkloadProps) => {
//   const [form] = Form.useForm();
//   const { Panel } = Collapse;

//   const onFinish = (value) => {
//     console.log(value);
//     console.log(form);
//   };

//   const onSubmitFormValues = () => {
//     console.log(form);
//     form.submit();
//     form.validateFields(['computeResource']).then((isValid) => console.log(isValid));
//     console.log(form.getFieldsValue());
//   }

//   const {data} = props;

  // return (    
  //     <>
  //       <Form 
  //         form={form} 
  //         name="form-workload" 
  //         onFinish={onFinish} 
  //         autoComplete="off" 
  //         layout={'vertical'}>
  //           <LayoutWrapper bgTransparent customClassNames="m-3">
  //             <Row className="p-3 box-shadow-thin">
  //               <Col md={16} sm={24} xs={24}>
  //                 {/* <Form.Item name="cloudProviders" label={i18next.t('labelCloudServiceProviders')} noStyle> */}
  //                     <CheckBoxGroup 
  //                       options={[{label: 'AWS', value: 'AWS'}, {label: 'Azure', value: 'Azure'}]} 
  //                       className='ml-2'
  //                       customProperties={{
  //                         showLabel: true,
  //                         label: i18next.t('labelCloudServiceProviders')
  //                       }}  
  //                       name="cloudProviders" 
  //                       defaultValue={data.cloudProviders}
  //                       onChange={}
  //                       />
  //                 {/* </Form.Item> */}
  //               </Col>
  //               <Col md={8} sm={24} xs={24} className={'text-md-right text-sm-left text-xs-left mt-sm-4 mt-md-0 mt-lg-0 mt-xl-0'}>
  //                 {/* <Form.Item name="tcoTerm" label={i18next.t('labelTCOTermYear')} noStyle> */}
  //                     <NumberSpinner 
  //                       min={1} 
  //                       max={5} 
  //                       size={'small'} 
  //                       className={'ml-2'} 
  //                       customProperties={{
  //                         showLabel: true,
  //                         label: i18next.t('labelTCOTermYear')
  //                       }}
  //                       name="tcoTerm"
  //                       defaultValue={data.tcoTerm}
  //                     />
  //                 {/* </Form.Item> */}
  //               </Col>
  //             </Row>
  //           </LayoutWrapper>

  //           <LayoutWrapper bgTransparent customClassNames="m-3">
  //             <Collapse bordered={false} defaultActiveKey={['1']}>
  //               <Panel header={i18next.t('headerComputeResources')} key="1">
  //                 <ComputeResource />
  //               </Panel>
  //             </Collapse>
  //           </LayoutWrapper>
            

  //         {/* <Form.Item>
  //           <Button type="primary" htmlType="submit">
  //             Submit
  //           </Button>
  //         </Form.Item> */}
  //       </Form>

  //       {/* <Form.Item>
  //         <Button type="primary" htmlType="submit" onClick={onSubmitFormValues}>
  //           Submit
  //         </Button>
  //       </Form.Item> */}
  //     </>
  // );
// }

// export default DefineWorkload;
