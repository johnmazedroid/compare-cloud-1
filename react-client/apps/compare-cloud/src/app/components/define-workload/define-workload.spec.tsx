import React from 'react';
import { render } from '@testing-library/react';

import DefineWorkload from './define-workload';

describe('DefineWorkload', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DefineWorkload />);
    expect(baseElement).toBeTruthy();
  });
});
