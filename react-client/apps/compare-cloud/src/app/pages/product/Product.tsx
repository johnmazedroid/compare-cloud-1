import React, { Component } from 'react';
import { Container } from 'react-bootstrap';

import './Product.scss';

export interface ProductProps {}

class Product extends Component<ProductProps> {

  render() {
    return (
      <Container fluid>
          Welcome to Product Page!!
       </Container>
    );
  }
}

export default Product;
