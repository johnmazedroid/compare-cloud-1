import React, { Component } from 'react';
import { Container } from 'react-bootstrap';

import './Help.scss';

export interface HelpProps {}

class Help extends Component<HelpProps> {
  render() {
    return (
      <Container fluid>
        <p>Welcome to help !</p>
      </Container>
    );
  }
}

export default Help;
