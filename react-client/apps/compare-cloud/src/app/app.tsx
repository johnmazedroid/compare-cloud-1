import React from 'react';
import { BrowserRouter as Router, HashRouter } from 'react-router-dom';
import { Layout } from 'antd';
import i18next from 'i18next';

import Menus from './components/menus/Menus';
import Routes from './components/routes/Routes';

import './App.scss';

const App = () => {
  const { Content, Footer } = Layout;
  return (
    <>
      <div data-automation-id="cloud-compare-app" id="cloud-compare-app">
        <React.StrictMode>
          <HashRouter>
              <Layout className="cloud-compare-layout">
                <Menus></Menus>
                <Content className="cloud-compare-main-body mx-4 mt-4 mb-0">
                  <Routes />
                </Content>
                <Footer>{i18next.t('footerText')}</Footer>
              </Layout>
          </HashRouter>
        </React.StrictMode>
      </div>
    </>
  );
};

export default App;
