import {
    WORKLOAD_OBJECT
} from './../constants/types';

const initialState = {
    data: {},
    errors: {} || '',
    loading: false
};

export const workLoadReducer = ( state = initialState, action ) => {
    // console.log('workLoadReducer ::', state, action);
    switch( action.type ) {
        case WORKLOAD_OBJECT:
        return {
            ...state,
            data: action.data,
            errors: action.errors,
            loading: action.loading
        };
        default:
            return state;
    }
}
