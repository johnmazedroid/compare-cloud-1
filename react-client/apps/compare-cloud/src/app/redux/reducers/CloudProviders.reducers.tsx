import {
    GET_CLOUD_PROVIDERS_LIST_REQUEST,
    GET_CLOUD_PROVIDERS_LIST_RESPONSE,
    GET_CLOUD_PROVIDERS_LIST_ERRORS
} from './../constants/types';

const initialState = {
    data: [],
    errors: {} || '',
    loading: false
};

export const cloudProviderReducer = ( state = initialState, action ) => {
    // console.log('cloudProviderReducer called ::', state, action);
    switch( action.type ) {
        case GET_CLOUD_PROVIDERS_LIST_REQUEST:
        return {
            ...state,
            data: action.data,
            errors: action.errors,
            loading: action.loading
        };
        case GET_CLOUD_PROVIDERS_LIST_RESPONSE:
        return {
            ...state,
            data: action.data,
            errors: action.errors,
            loading: action.loading
        };
        case GET_CLOUD_PROVIDERS_LIST_ERRORS:
        return {
            ...state,
            data: action.data,
            errors: action.errors,
            loading: action.loading
        };
        default:
            return state;
    }
}
