import {
  GET_INSTANCE_FAMILY_LIST_REQUEST,
  GET_INSTANCE_FAMILY_LIST_RESPONSE,
  GET_INSTANCE_FAMILY_LIST_ERRORS
} from './../constants/types';
import {getDataByAttributeNameHTTPService} from '@react-client/shared';

const instanceFamilyRequest = () => {
  // console.log('instanceFamilyRequest called');
  return {
    type: GET_INSTANCE_FAMILY_LIST_REQUEST,
    data: [],
    errors: {},
    loading: true
  }
}

const getInstanceFamilyResponse = (data) => {
  // console.log('getInstanceFamilyResponse called :::', data);
  return {
    type: GET_INSTANCE_FAMILY_LIST_RESPONSE,
    data,
    errors: {},
    loading: false
  }
}

const getInstanceFamilyError = (error) => {
  // console.log('getInstanceFamilyError called :::', error);
  return {
    type: GET_INSTANCE_FAMILY_LIST_ERRORS,
    data: [],
    errors: error,
    loading: false
  }
}

export const getInstanceFamily = (instanceFamilyContext) => (dispatch) => {
  // console.log('getInstanceFamily called');
  dispatch(instanceFamilyRequest())
  getDataByAttributeNameHTTPService(instanceFamilyContext)
    .then((response) => {
      dispatch(getInstanceFamilyResponse(response.data));
    })
    .catch((error) => {
      dispatch(getInstanceFamilyError(error))
    })
}
