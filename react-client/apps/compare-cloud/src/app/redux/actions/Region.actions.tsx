import {
  GET_REGION_LIST_REQUEST,
  GET_REGION_LIST_RESPONSE,
  GET_REGION_LIST_ERRORS
} from './../constants/types';
import {getDataByAttributeNameHTTPService} from '@react-client/shared';

const regionRequest = () => {
  // console.log('regionRequest called');
  return {
    type: GET_REGION_LIST_REQUEST,
    data: [],
    errors: {},
    loading: true
  }
}

const getRegionResponse = (data) => {
  // console.log('getRegionResponse called :::', data);
  return {
    type: GET_REGION_LIST_RESPONSE,
    data,
    errors: {},
    loading: false
  }
}

const getRegionError = (error) => {
  // console.log('getRegionError called :::', error);
  return {
    type: GET_REGION_LIST_ERRORS,
    data: [],
    errors: error,
    loading: false
  }
}

export const getRegion = (regionContext) => (dispatch) => {
  // console.log('getRegion called');
  dispatch(regionRequest())
  getDataByAttributeNameHTTPService(regionContext)
    .then((response) => {
      dispatch(getRegionResponse(response.data));
    })
    .catch((error) => {
      dispatch(getRegionError(error))
    })
}
