import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import commonEN from './assets/i18n/common_en.json';
import compareCloudEN from './assets/i18n/compare-cloud_en.json';

// the translations
const resources = {
  en: {
    translation: compareCloudEN,
    common: commonEN
  }
};

i18n
  .use(initReactI18next)
  .init({
    resources,
    lng: "en",

    keySeparator: false,

    interpolation: {
      escapeValue: false
    },

    ns: ['common'],
  });

export default i18n;