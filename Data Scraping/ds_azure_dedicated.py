import re
import sys
import time
import json
import requests
import ds_database
import mysql.connector
from bs4 import BeautifulSoup

start_time = time.time()
cloud_provider = '2'
instance_type = 'Dedicated'
operating_system = ''
default_storage = ''
instance_family = ''
offering_class = 'Standard'
payment_option = 'No Upfront'

# Initialize database
db = ds_database.Database(cloud_provider, instance_type)

# For debugging
# file = open('azure_pricing_dedicatedhost.csv', 'w')

server_attributes_list = []
server_pricing_list = []

url = 'https://azure.microsoft.com/en-us/pricing/details/virtual-machines/dedicated-host/'
print('Fetching data for URL: ' + url)

try:
	response = requests.get(url)
except:
	print('Could not fetch data for ' + url)
	cursor.close()
	cc_db.close()
	sys.exit()

soup = BeautifulSoup(response.text, 'html.parser')
# rows = soup.select("section[aria-label='Azure Dedicated Host pricing details'] > div > div[class='sd-table-container'] > table > tbody")
rows = soup.select("section[aria-label='Azure Dedicated Host pricing details'] > div > div[class='row row-size1 column'] > div > table > tbody > tr")

for row in rows:

	td = row.find_all('td')
	try:
		instance_name = td[0].get_text().strip()
		virtual_cpu = td[1].get_text().strip()

		memory_list = re.findall('\d+\.\d+', td[2].get_text().strip())
		if not memory_list:
			memory_list = re.findall('\d', td[2].get_text().strip())

		memory = memory_list[0]
		processor = td[3].get_text().strip()

	except:
		print('Data could not be parsed for ' + str(td))
		continue # move on to next row

	spans = row.find_all('span', {'class': 'price-data'})
	ondemand = json.loads(spans[0]['data-amount'])['regional']

	try:
		yr1_reserved = json.loads(spans[1]['data-amount'])['regional']
	except:
		yr1_reserved = {}
		# print('1 yr reserved price not available for ' + instance_name)

	try:
		yr3_reserved = json.loads(spans[2]['data-amount'])['regional']
	except:
		yr3_reserved = {}
		# print('3 yr reserved price not available for ' + instance_name)

	for key in ondemand:
		region = key

		lease_term = 'On Demand'
		ondemand_price = ondemand[key]
		price_tuple = (cloud_provider, operating_system, region, instance_name, lease_term, offering_class, payment_option, ondemand_price)
		server_pricing_list.append(price_tuple)

		if key in yr1_reserved:
			lease_term = '1yr'
			yr1_reserved_price = yr1_reserved[key]
			price_tuple = (cloud_provider, operating_system, region, instance_name, lease_term, offering_class, payment_option, yr1_reserved_price)
			server_pricing_list.append(price_tuple)

		if key in yr3_reserved:
			lease_term = '3yr'
			yr3_reserved_price = yr3_reserved[key]
			price_tuple = (cloud_provider, operating_system, region, instance_name, lease_term, offering_class, payment_option, yr3_reserved_price)
			server_pricing_list.append(price_tuple)

		attr_tuple = (cloud_provider, operating_system, region, instance_name, virtual_cpu, memory, default_storage, instance_type, instance_family, processor)
		server_attributes_list.append(attr_tuple)

try:
	db.cursor.executemany(db.server_attributes_ins_sql, server_attributes_list)
	db.cursor.executemany(db.server_pricing_ins_sql, server_pricing_list)
	db.connect.commit()
	print('Data inserted successfully.')
except (mysql.connector.Error, mysql.connector.Warning) as e:
	print(e)

# For debugging
# file.write(str(server_attributes_list))
# file.close()

db.cursor.close()
db.connect.close()

elapsed_seconds = round((time.time() - start_time))
print('Execution Time: ' + time.strftime("%H:%M:%S", time.gmtime(elapsed_seconds)) + ' (hh:mm:ss)')
