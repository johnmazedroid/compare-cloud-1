import time
import mysql.connector
import ds_database

def ExecuteSQLFromFile(file_name):
	fd = open(file_name, 'r')
	sql_file = fd.read()
	fd.close()

	sql_commands = sql_file.split(';')
	for command in sql_commands:
		try:
			db.cursor.execute(command)
		except (mysql.connector.Error, mysql.connector.Warning) as e:
			print(e)

def ExecuteStep(step_description, step_sql_file_name):
	step_start_time = time.time()
	ExecuteSQLFromFile(step_sql_file_name)
	elapsed_seconds = round((time.time() - step_start_time))
	print(step_description + '. Execution Time: ' + time.strftime("%H:%M:%S", time.gmtime(elapsed_seconds)) + ' (hh:mm:ss)')
	print('====================================================================')

start_time = time.time()

# Initialize database
db = ds_database.Database("", "")

# Step 1 - Insert master tables
ExecuteStep('Step 1 - Insert master tables', '1_insert_master_tables.sql')
db.connect.commit()

# Step 2 - Update master ids in staging tables
ExecuteStep('Step 2 - Update master ids', '2_update_master_ids.sql')
db.connect.commit()

# Step 3 - Create temp tables
ExecuteStep('Step 3 - Create temp tables', '3_create_tmp_tables.sql')

# Step 4 - Insert temp tables
ExecuteStep('Step 4 - Insert temp tables', '4_insert_tmp_tables.sql')
db.connect.commit()

# Step 5 - Rename temp tables
ExecuteStep('Step 5 - Rename temp tables', '5_rename_tmp_tables.sql')
db.connect.commit()

# End program
db.cursor.close()
db.connect.close()

elapsed_seconds = round((time.time() - start_time))
print('Total Execution Time: ' + time.strftime("%H:%M:%S", time.gmtime(elapsed_seconds)) + ' (hh:mm:ss)')