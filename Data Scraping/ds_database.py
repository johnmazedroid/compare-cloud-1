import json
import mysql.connector

class Database:
	def __init__(self, cloud_provider, instance_type):

		# Read configuration file
		with open('ds_config.cfg') as config:
			config_params = json.load(config)

		# Initialize database connection
		self.connect = mysql.connector.connect(
			host = config_params['db_credentials']['host'],
			database = config_params['db_credentials']['database'],
			user = config_params['db_credentials']['user'],
			password = config_params['db_credentials']['password']
		)
		self.cursor = self.connect.cursor()
		self.server_attributes_ins_sql = "INSERT INTO server_attributes_stg(cloud_provider_id, operating_system, region, instance_name, virtual_cpu, memory, default_storage, instance_type, instance_family, processor) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
		self.server_pricing_ins_sql = "INSERT INTO server_pricing_stg(cloud_provider_id, operating_system, region, instance_name, lease_term, offering_class, payment_option, hourly_price) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"

		# Cleanup server attributes and pricing tables
		server_attributes_del_sql = "DELETE FROM server_attributes_stg WHERE cloud_provider_id = %s AND instance_type = %s"
		server_pricing_del_sql = "DELETE FROM server_pricing_stg WHERE (cloud_provider_id, operating_system, region, instance_name) IN (SELECT cloud_provider_id, operating_system, region, instance_name FROM server_attributes_stg WHERE cloud_provider_id = %s AND instance_type = %s)"

		if cloud_provider:
			try:
				self.cursor.execute(server_pricing_del_sql, (cloud_provider, instance_type))
				self.cursor.execute(server_attributes_del_sql, (cloud_provider, instance_type))
			except (mysql.connector.Error, mysql.connector.Warning) as e:
				print(e)
